<?php

/* 
 * Todo list
 * Test task for Avia Solutions Group
 * by Povilas Brilius 2014
 */
return [
    'bjyauthorize' => [

        // set the 'guest' role as default (must be defined in a role provider)
        'default_role' => 'guest',

        // resource providers provide a list of resources that will be tracked
        // in the ACL. like roles, they can be hierarchical
        'resource_providers' => [
            \BjyAuthorize\Provider\Resource\Config::class => [
                'users' => [],
				'tasks' => [],
            ],
        ],

        /* rules can be specified here with the format:
         * [roles (array], resource, [privilege (array|string], assertion])
         * assertions will be loaded using the service manager and must implement
         * Zend\Acl\Assertion\AssertionInterface.
         * *if you use assertions, define them using the service manager!*
         */
        'rule_providers' => [
            \BjyAuthorize\Provider\Rule\Config::class => [
                'allow' => [
                    // allow guests and users (and admins, through inheritance)
                    // the "wear" privilege on the resource "pants"
                    [['user'], 'tasks', 'manage'],
					[['admin'], 'users', 'administer'],
                ],

                // Don't mix allow/deny rules if you are using role inheritance.
                // There are some weird bugs.
                'deny' => [
                    // ...
                ],
            ],
        ],

        /* Currently, only controller and route guards exist
         *
         * Consider enabling either the controller or the route guard depending on your needs.
         */
        'guards' => [
            /* If this guard is specified here (i.e. it is enabled], it will block
             * access to all controllers and actions unless they are specified here.
             * You may omit the 'action' index to allow access to the entire controller
             */
            \BjyAuthorize\Guard\Controller::class => [
                ['controller' => 'Todo\Controller\Todo', 'action' => 'index', 'roles' => ['guest']],
                ['controller' => 'Todo\Controller\Todo', 'action' => 'tasks', 'roles' => ['user']],
				['controller' => 'Todo\Controller\Todo', 'action' => 'users', 'roles' => ['admin']],
				['controller' => 'Todo\Controller\Todo', 'action' => 'showform', 'roles' => ['user']],
				['controller' => 'Todo\Controller\Todo', 'action' => 'validatepostajax', 'roles' => ['user']],
				['controller' => 'Todo\Controller\Todo', 'action' => 'deletetask', 'roles' => ['user']],
				['controller' => 'Todo\Controller\Todo', 'action' => 'getsessiontimeout', 'roles' => ['user']],
				['controller' => 'Todo\Controller\Todo', 'action' => 'extendsession', 'roles' => ['user']],
                // You can also specify an array of actions or an array of controllers (or both)
                // allow "guest" and "admin" to access actions "list" and "manage" on these "index",
                // "static" and "console" controllers
				/*
                [
                    'controller' => ['index', 'static', 'console'],
                    'action' => ['list', 'manage'],
                    'roles' => ['guest', 'admin'],
                ],
                [
                    'controller' => ['search', 'administration'],
                    'roles' => ['staffer', 'admin'],
                ],

				 */
                ['controller' => 'zfcuser', 'roles' => []],
				['controller' => 'SymfonyConsoleModule\Controller\Console', 'roles' => []],
				['controller' => 'ZFTool\Controller\Create', 'roles' => []],

                // Below is the default index action used by the ZendSkeletonApplication
                // ['controller' => 'Application\Controller\Index', 'roles' => ['guest', 'user']],
            ],

            /* If this guard is specified here (i.e. it is enabled], it will block
             * access to all routes unless they are specified here.
             */
            \BjyAuthorize\Guard\Route::class => [
                ['route' => 'zfcuser', 'roles' => ['user']],
                ['route' => 'zfcuser/logout', 'roles' => ['user']],
				['route' => 'zfcuser/changepassword', 'roles' => ['user']],
                ['route' => 'zfcuser/login', 'roles' => ['guest']],
                ['route' => 'zfcuser/register', 'roles' => ['guest']],
                ['route' => 'home', 'roles' => ['guest']],
				['route' => 'todo', 'roles' => ['guest']],
				['route' => 'symfony-console-module', 'roles' => ['guest']],
				['route' => 'zftool-create-module', 'roles' => ['guest']]
            ],
        ],
    ],
];

