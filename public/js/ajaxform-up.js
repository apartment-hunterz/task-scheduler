$(function(){
    $("form#Task").submit(function(e){        
        //if not call by ajax
        //submit to showformAction
        e.preventDefault();
        if (is_xmlhttprequest == 0){
            return true;
        }
        $.post(validateajax, {
               title : $('#Task input[name="title"]').val(),
               dueDate : $('#Task input[name="dueDate"]').val(),
               priority : $('#Task input[name="priority"]').val(),
               status : $('#Task select[name="status"] option:selected').val()
            }, function(itemJson){  
                var error = false;
                for (var key in itemJson){
                    if (key == 'success'){
                        break;
                    }
                    if ($('.element_' + key + ' ul').length == 0){
                        $('.element_' + key).append('<ul></ul>');
                    }
                    else{
                       $('.element_' + key + ' ul').html(''); 
                    }
                    for (var a = 0; a < itemJson[key].length; a++){
                        $('.element_' + key + ' ul').append('<li>' + itemJson[key][a] + '</li>');
                    }
                    error = true;
                }
                if (!error){
                    $("#winPopup").dialog('close');
                    if (itemJson.success == 1){
                        alert('Data saved');  
                    }
                }   
        }, 'json');
         
        return false;
    });
});    