/* 
 * Todo list
 * Test task for Avia Solutions Group
 * by Povilas Brilius 2014
 */
$(function(){
    $("#newTask").click(function(e){
        e.preventDefault();
        $("#winPopup").dialog({
            draggable:true,
            modal: true,
            autoOpen: false,
           // height:300,
            width:400,
            //resizable: false,
            title:'New task'
           // position:'center'
        });
        $("#winPopup").load($(this).attr('href'), function(){
            $('#Task input[name="dueDate"]').attr('placeholder', 'Due date').datetimepicker({
                dayOfWeekStart : 1,
                lang:'en',
                format:'Y-m-d H:i'
            });
            
        });
        $("#winPopup").dialog("open");
    });
    
    $('#Tasks a[role="menuitem"]').on('click', function(e){
        e.preventDefault();
        var row = $(this).parents('tr');
        var taskId = row.attr('id').substr(3);
        $('#winPopup').html('');
        if ($(this).hasClass('deleteTask')){
            $("#dialog-confirm" ).dialog({
                resizable: false,
                height:180,
                width:400,
                modal: true,
                buttons: {
                    'Delete the task': function() {
                        var dialog = $(this);
                        $.post(delTaskUrl + '/' + taskId, {
                            taskId: taskId
                        }, function(response){
                            if (response.success == true) {
                                row.fadeOut(400, function(){
                                    row.remove();
                                });
                                dialog.dialog('close');
                            }
                        }, 'json');
                    },
                    Cancel: function() {
                        $(this).dialog('close');
                    }
                }
            });
        }
        if ($(this).hasClass('editTask')){
            $("#winPopup").dialog({
                draggable:true,
                modal: true,
                autoOpen: false,
               // height:300,
                width:400,
                //resizable: false,
                title:'Edit task'
               // position:'center'
            });
            $("#winPopup").load(formUrl + '/' +taskId, function(){
                $('#Task input[name="dueDate"]').attr('placeholder', 'Due date').datetimepicker({
                    dayOfWeekStart : 1,
                    lang:'en',
                    format:'Y-m-d H:i'
                });

            });
            $("#winPopup").dialog("open");
        }
    });
    function checkSessionExpiration()
    {
        if (!loggedInUser){
            return;
        }
        $.get(checkSessionUrl, {}, function(response){
            if (response.secondsLeft > 900){
                return;
            }
            $('#sessionConfirm').dialog({
                resizable: false,
                height:180,
                width:400,
                modal: true,
                buttons: {
                    Ok: function() {
                        var dialog = $(this);
                        $.get(extendSessionUrl, {}, function(response){
                            if (response.success == true) {
                                dialog.dialog('close');
                            }
                        }, 'json');
                    }
                }
            });
        }, 'json');
    }
    setInterval(checkSessionExpiration, 60000);
});

