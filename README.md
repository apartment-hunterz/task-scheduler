#Application

A calendar with prioritezed tasks organizer.
Based on Zend Framework2, Doctrine ORM, Doctrine Data Fixtures, ZfcUser,
 ByjAuthorize modules and Zend\Annotation with Doctrine\Entity usage for
 Zend\Form composition, rendering, processing and recording. 

#Installation

*
```
composer install
cp config/autoload/local.php.dist config/autoload/local.php
```
* Create tasklist database
```
mysql -e 'create database tasklist' -uroot -p
```
* Import msyql database
```
cat db/tasklist.sql > mysql -u root -p tasklist
```

#Note

The current database import is crude enough and simplified due to the missing
Doctrine Migrations and extended Doctrine ORM mapping in source folder