<?php

namespace Todo\Model;

use Doctrine\ORM\EntityManager;
use Task\Entity\Task;
use User\Entity\User;
use Zend\Authentication\AuthenticationService;
use Zend\Form\Annotation\AnnotationBuilder;

class Todo
{
	/**
	 * Form tasks table
	 *  
	 * @param EntityManager $em
	 * @param AuthenticationService $authService
	 * @return array
	 */
    public function displayTasksTable(EntityManager $em, AuthenticationService $authService)
    {
        $todaysTasks = $em->getRepository(Task::class)->getTodaysTasks($authService->getIdentity()->getId());
		$lateTasks = $em->getRepository(Task::class)->getLateTasks($authService->getIdentity()->getId());
		$highestPriorityTasks = $em->getRepository(Task::class)->getHighestPriorityTasks($authService->getIdentity()->getId());
		$allUserTasks = $em->getRepository(Task::class)->getUserTasks($authService->getIdentity()->getId());
		$tables = [];
		for ($a = 0; $a < 4; $a++){
			$tables[] = [
				'headers' => ['Title', 'Due date', 'Priority', 'Status'],
			];
		}
		$tables[0]['name'] = 'Today\'s tasks';
		$tables[0]['rows'] = $todaysTasks;
		$tables[0]['rowIdPrefix'] = 'TDT';
		
		$tables[1]['name'] = 'Late tasks';
		$tables[1]['rows'] = $lateTasks;
		$tables[1]['rowIdPrefix'] = 'LTT';
		
		$tables[2]['name'] = 'Highest priority tasks';
		$tables[2]['rows'] = $highestPriorityTasks;
		$tables[2]['rowIdPrefix'] = 'HPT';
		
		$tables[3]['name'] = 'All your tasks';
		$tables[3]['rows'] = $allUserTasks;
		$tables[3]['rowIdPrefix'] = 'AUT';
		for ($a = 0; $a < count($tables); $a++){
			$tables[$a]['rowDropDown'] = [
				['class' => 'editTask', 'name' => 'Edit task'],
				['class' => 'deleteTask', 'name' => 'Delete task']
			];
		}

		return $tables;
    }

    /**
	 * Show form
	 *  
	 * @param string $id
	 * @param EntityManager $em
	 * @param AuthenticationService $authService
	 * @return Zend\Form
	 */
	public function getForm($id, EntityManager $em, AuthenticationService $authService)
    {
		if (!empty((int)$id)){
			$authService = $this->getServiceLocator()->get('zfcuser_auth_service');
			$em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
			$task = $em->getRepository(Task::class)->getTask($id, $authService->getIdentity()->getId(), true);
			$task = $task[0];
		}
		$entity     = new Task();
        $builder    = new AnnotationBuilder();
        $form       = $builder->createForm($entity);
        $form->get('title')->setAttribute('class', 'form-control')
				->setAttribute('placeholder', 'Title');
        $form->get('dueDate')->setAttribute('class', 'form-control');
        $form->get('dueDate')->setAttribute('placeholder', 'Due date');
        $form->get('priority')->setAttribute('class', 'form-control')
				->setAttribute('placeholder', 'Priority')
				->setAttribute('min', '-1000')
				->setAttribute('max', '1000')
				->setAttribute('step', '1');
		$form->get('status')->setAttribute('class', 'form-control');
        $form->get('submit')->setAttribute('class', 'btn btn-lg btn-primary btn-block');
		
		if (!empty((int)$id)){
			$form->setData([
				'title' => $task['title'],
				'dueDate' => $task['dueDate'],
				'priority' => $task['priority'],
				'status' =>  array_search($task['status'], Task::$Statuses)
			]);
		}
		
        return $form;
    }

    /**
	 * Save form data to db
	 * 
	 * @param array $data
	 * @param string $id
	 * @param EntityManager $em
	 * @param AuthenticationService $authService
	 * @return bool
	 */
	public function saveToDb($data, $id, EntityManager $em, AuthenticationService $authService)
    {
		if (empty($id)){
			$task = new Task();
			$task->setTitle($data['title'])
				->setDueDate((new \DateTime())->createFromFormat('Y-m-d H:i', $data['dueDate']))
				->setPriority((int)$data['priority'])
				->setStatus($task::$Statuses[(int)$data['status']])
				->setUserId($authService->getIdentity()->getId());
		} else {
			$task = $em->getRepository(Task::class)->getTask($id, $authService->getIdentity()->getId(), false);
			$task = $task[0];
			if (empty($task)) {
				return;
			}
			$task->setTitle($data['title'])
				->setDueDate((new \DateTime())->createFromFormat('Y-m-d H:i', $data['dueDate']))
				->setPriority((int)$data['priority'])
				->setStatus($task::$Statuses[(int)$data['status']]);
		}
		$em->persist($task);
		return $em->flush();
    }

     /**
	 * Form users table
	 * 
	 * @param EntityManager $em
	 * @return array
	 */
    public function displayUsers(EntityManager $em)
    {
    	$users = $em->createQuery('SELECT u FROM ' . User::class . ' u ORDER BY u.displayName')->getResult();
		$res = [];
		foreach ($users as $u) {
			$roles = $u->getRoles();
			$res[] = [
				'displayName' => $u->getDisplayName(),
				'email' => $u->getEmail(),
				'role' => $roles[0]->getRoleId()
			];
		}

		return $res;
    }

}