<?php

namespace Todo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Task\Entity\Task;
use Zend\Json\Json;
use Zend\Session\Container;
use Todo\Model\Todo;

class TodoController extends AbstractActionController
{
    public function indexAction()
    {
        if ($this->isAllowed('users', 'administer')) {
			$this->redirect()->toRoute('todo', array('action'=>'users'));
		}
		elseif ($this->isAllowed('tasks', 'manage')) {
			$this->redirect()->toRoute('todo', array('action'=>'tasks'));
		}
		else {
			$this->redirect()->toRoute('zfcuser/login');
		}
    }
	
	public function tasksAction()
	{
		$authService = $this->getServiceLocator()->get('zfcuser_auth_service');
		$em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		$tables = (new Todo())->displayTasksTable($em, $authService);
		
		return new ViewModel([
			'tables' => $tables
		]);
	}
	
	public function usersAction()
	{
		$em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		$users = (new Todo())->displayUsers($em);
		return new ViewModel([
			'users' => $users
		]);
	}
	     
    public function showformAction()
    {
        $viewModel = new ViewModel();
        $request = $this->getRequest();

		$id = $this->params()->fromRoute('id');
		$authService = $this->getServiceLocator()->get('zfcuser_auth_service');
		$em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		$form = (new Todo())->getForm($id, $em, $authService);
		
        //disable layout if request by Ajax
        $viewModel->setTerminal($request->isXmlHttpRequest());
         
        $is_xmlhttprequest = 1;
        if (!$request->isXmlHttpRequest()){
            //if NOT using Ajax
            $is_xmlhttprequest = 0;
            if ($request->isPost()){
                $form->setData($request->getPost());
                if ($form->isValid()){
                    //save to db 
                    (new Todo())->saveToDb($form->getData(), $id, $em, $authService);
                }
            }
        }
		$title = !empty($id) ? 'Edit task' : 'New task';
        $viewModel->setVariables(array(
			'form' => $form,
			'title' => $title,
			'id' => $id,
			// is_xmlhttprequest is needed for check this form is in modal dialog or not
			// in view
			'is_xmlhttprequest' => $is_xmlhttprequest
        ));
         
        return $viewModel;
    }
     
    public function validatepostajaxAction()
    {
		$id = $this->params()->fromRoute('id');
		$authService = $this->getServiceLocator()->get('zfcuser_auth_service');
		$em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		$todo = new Todo();
        $form    = $todo->getForm($id, $em, $authService);
        $request = $this->getRequest();
        $response = $this->getResponse();
		
        $messages = array();
        if ($request->isPost()){
            $form->setData($request->getPost());
            if (!$form->isValid()){
                $errors = $form->getMessages();
                foreach($errors as $key=>$row){
                    if (!empty($row) && $key != 'submit'){
                        foreach($row as $keyer => $rower){
                            //save error(s) per-element that
                            //needed by Javascript
                            $messages[$key][] = $rower;   
                        }
                    }
                }
            }
             
            if (!empty($messages)){       
                $response->setContent(Json::encode($messages));
            }
			else{
                //save to db
                $todo->saveToDb($form->getData(), $id, $em, $authService);
                $response->setContent(Json::encode(array('success' => 1)));
            }
        }
         
        return $response;
    }
	
	/**
	 * Delete task
	 * 
	 * @param string $id
	 * @return Response
	 */
	public function deletetaskAction()
	{
		$request = $this->getRequest();
        $response = $this->getResponse();
		$id = $this->params()->fromRoute('id');
		if (empty($id)) {
			$response->setStatusCode(404);
			return $response;
		}
		$em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		$authService = $this->getServiceLocator()->get('zfcuser_auth_service');
		$success = $em->getRepository(Task::class)->deleteTask((int)$id, $authService->getIdentity()->getId());
		$response->setContent(Json::encode(['success' => (bool) $success]));
		return $response; 
	}
	
	/**
	 * Rturns remaining session time in seconds
	 * 
	 * @return Response
	 */
	public function getsessiontimeoutAction()
	{
        $response = $this->getResponse();
		$container = new Container('authentication');
		$datetime = new \DateTime();
		$diff = $datetime->diff($container->sessionExpire);
		$seconds = $diff->s + $diff->i*60 + $diff->h*60*60; 
		$response->setContent(Json::encode(['secondsLeft' => $seconds]));
		return $response; 
	}
	
	/**
	 * Extends session by an hour
	 * 
	 * @return Response
	 */
	public function extendsessionAction()
	{
		$sessionManager = $this->getServiceLocator()->get('Zend\Session\SessionManager');
		$sessionManager->getConfig()->setStorageOption('remember_me_seconds', '3600');
		$sessionManager->getConfig()->setStorageOption('gc_maxlifetime', '3600');
		$sessionManager->regenerateId(false);
		$rememberMe = $sessionManager->getConfig()->getStorageOption('remember_me_seconds');
		$container = new Container('authentication');
		$container->sessionExpire = (new \DateTime('+' . $rememberMe . ' seconds'));
		$response = $this->getResponse();
		$response->setContent(Json::encode(['success' => true]));
		return $response;
	}
}
