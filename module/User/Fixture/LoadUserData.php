<?php

namespace User\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use User\Entity\Role;
use User\Entity\User;
use Zend\Crypt\Password\Bcrypt;
use ZfcUser\Options\ModuleOptions;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $guestRole = new Role();
        $guestRole->setRoleId('guest');
        $manager->persist($guestRole);
        $manager->flush();
        
        $userRole = new Role();
        $userRole->setRoleId('user');
        $userRole->setParent($guestRole);
        $manager->persist($userRole);
        $manager->flush();
        
        $adminRole = new Role();
        $adminRole->setRoleId('admin');
        $adminRole->setParent($userRole);
        $manager->persist($adminRole);
        $manager->flush();
        
        $bcrypt = new Bcrypt();
        $options = new ModuleOptions();
        $bcrypt->setCost($options->getPasswordCost());
        
        $user = new User();
        $user->setEmail('test_user@taskslist.com');
        $user->setDisplayName('Test user1');
        $user->setPassword($bcrypt->create('test3333'));
        $user->addRole($userRole);
        $manager->persist($user);
        $manager->flush();
        
        $admin = new User();
        $admin->setEmail('test_admin@taskslist.com');
        $admin->setDisplayName('Test admin1');
        $admin->setPassword($bcrypt->create('test5555'));
        $admin->addRole($adminRole);
        $manager->persist($admin);
        $manager->flush();
    }
}
